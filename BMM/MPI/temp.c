#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define ERR_BADORDER    255
#define TAG_INIT        20
#define TAG_RESULT	42
int getRowCount(int rowsTotal, int rank, int sizem);
int matrixMultiply(double *A, double *B, double *C, int n, int local,int n1,int n2);

int main(int argc, char *argv[]) {

	int n = 0,n1=0,n2=0, upperBound,upperBoundc, local, totalSizeA,totalSizeB,totalSizeC, i;
	int rank=0, sizem;
	double *A, *B, *C;
	long t;
	int alreadysent, tosent;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &sizem);

	if (!rank)
	{
		if (argc > 1)
		{
			n = atoi(argv[1]);
			n1 = atoi(argv[2]);
			n2 = atoi(argv[3]);
		}

		if (!n)
			printf("Order not given\n");
	}

	MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&n1, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&n2, 1, MPI_INT, 0, MPI_COMM_WORLD);

	if (!n || !n1 || !n2)
	{
		MPI_Finalize();
		return ERR_BADORDER;
	}

	local  = getRowCount(n, rank, sizem);
	upperBound = n1 * local;
	upperBoundc= n2*local;
	totalSizeA     = n * n1;
	totalSizeB    = n1*n2;
	totalSizeC    = n*n2;

	A = (double *) malloc(sizeof(double) * (rank ? upperBound : totalSizeA));
	B = (double *) malloc(sizeof(double) * totalSizeB );
	C = (double *) malloc(sizeof(double) * (rank ? upperBoundc : totalSizeC));

	/* Initialize A and B using some functions */
	if (!rank) {
		for (i=0; i<totalSizeA; i++)
		{
			A[i] = 1.0;
		}
		for (i=0; i<totalSizeB; i++)
		{
			B[i] = 1.0;
		}
	}
	if(!rank)
		t = MPI_Wtime();

	/* Send A by splitting it in row-wise parts */
	if (!rank)
	{
		alreadysent = upperBound;
		for (i=1; i<sizem; i++)
		{
			tosent = n1 * getRowCount(n, i, sizem);
			MPI_Send(A + alreadysent, tosent, MPI_DOUBLE, i, TAG_INIT,MPI_COMM_WORLD);
			alreadysent += tosent;
		}
	}

	else { /* Receive parts of A */
		MPI_Recv(A, upperBound, MPI_DOUBLE, 0, TAG_INIT, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
	}

	/* Send B completely to each process */
	MPI_Bcast(B, n1*n2, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	/* Let each process initialize C to zero */
	for (i=0; i<upperBoundc; i++) {
		C[i] = 0.0;
	}

	/* Let each process perform its own multiplications */
	matrixMultiply(A, B, C, n, local,n1,n2);

	/* Receive partial results from each slave */
	if (!rank) {
		alreadysent = upperBoundc;
		for (i=1; i<sizem; i++) {
			tosent = n2 * getRowCount(n, i, sizem);
			MPI_Recv(C + alreadysent, tosent, MPI_DOUBLE, i, TAG_RESULT,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			alreadysent += tosent;
		}
	}
	else { /* Send partial results to master */
		MPI_Send(C, upperBoundc, MPI_DOUBLE, 0, TAG_RESULT, MPI_COMM_WORLD);
	}

	/* Stop timer, includes communication time */
	if(!rank)
		t = MPI_Wtime() - t;

	if(!rank)
		//printf("Total time for processor %d was %f seconds.\n", rank, t);
		printf("[INFO] EXECUTION TIME is %ld\n", t);

	/* Goodbye, world */
	MPI_Finalize();
	return 0;

}
/* Function to return the row count */
int getRowCount(int rowsTotal, int rank, int sizem) {
	if(rowsTotal % sizem !=0 && rank==0)
		return ((rowsTotal / sizem) + (rowsTotal % sizem));
	else
		return (rowsTotal / sizem);
}

int matrixMultiply(double *a, double *b, double *c, int n, int local,int n1,int n2) {
	int i, j, k;
	for (i=0; i<local; i++) {
		for (j=0; j<n2; j++) {
			for (k=0; k<n1; k++) {
				c[i*n + j] += a[i*n + k] * b[k*n1 + j];
			}
		}
	}
	return 0;
}
