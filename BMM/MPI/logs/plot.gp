#! /usr/bin/gnuplot

        set xlabel "Matrix Size"
        set ylabel "Execution time (ms)"
        set key left
        set term png
set style histogram cluster gap 1
set style fill solid 0.5
set boxwidth 0.9
set style histogram errorbars linewidth 1
set errorbars linecolor black
red = "#FF0000"; green = "#00FF00"; blue = "#0000FF"; skyblue = "#87CEEB" ; violet = "#FF00FF"; purple = "#440154" ;
set grid ytics
        set format y '10^{%L}'
        set logscale y
        set yrange [1:]
        set output "${FILENAME}.png"
        set datafile separator ","
        set style data histogram
        plot "BMMMPI-tfx2U18C48-2107221659.csv" using 2:3:4:xtic(int($0)%3==0 ? strcol(1):'') title "Execution time (ms)" linecolor rgb purple linewidth 0

