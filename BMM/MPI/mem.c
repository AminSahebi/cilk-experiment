/*
	SMPSs Superscalar (SMPSs) Fast Fourier Transform
	Copyright (C) 2010 Barcelona Supercomputing Center - Centro Nacional de Supercomputacion
	
	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.
	
	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.
	
	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
	
	The GNU Lesser General Public License is contained in the file COPYING.
*/


#ifndef _BSD_SOURCE
#define _BSD_SOURCE 1
#endif

#include <malloc.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>

static _Bool initialized = 0;
static size_t page_size;

static void init() {
	if (!initialized) {
		page_size = sysconf(_SC_PAGESIZE);
		initialized = 1;
	}
}


void aligned_free(void *data, size_t size) {
	init();
	
	size_t number_of_pages = (size + page_size - 1L) / page_size;
	size_t corrected_size = number_of_pages * page_size;
	
	int rc = munmap(data, corrected_size);
	if (rc != 0) {
		perror("aligned_free::munmap3");
		exit(1);
	}
}


void *aligned_malloc(size_t size) {
	init();
	
	size_t number_of_pages = (size + page_size - 1L) / page_size;
	size_t corrected_size = number_of_pages * page_size;
	
	void *new_memory = mmap(0, corrected_size * 2L, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
	if (new_memory == MAP_FAILED) {
		perror("aligned_malloc");
		exit(1);
	}
	
	void *new_aligned_memory;
	size_t new_memory_unalignment = (size_t)new_memory % corrected_size;
	
	if (new_memory_unalignment == 0L) {
		new_aligned_memory = new_memory;
		munmap(new_memory + corrected_size, corrected_size);
	} else {
		new_aligned_memory = new_memory + corrected_size - new_memory_unalignment;
		int rc = munmap(new_memory, new_aligned_memory - new_memory);
		if (rc != 0) {
			perror("aligned_malloc::munmap1");
			exit(1);
		}
		rc = munmap(
			new_aligned_memory + corrected_size,
			new_memory + 2L * corrected_size - (new_aligned_memory + corrected_size));
		if (rc != 0) {
			perror("aligned_malloc::munmap2");
			exit(1);
		}
	}
	
	return new_aligned_memory;
}

