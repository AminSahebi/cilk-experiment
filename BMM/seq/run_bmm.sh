#!/bin/bash

if [ "$2" = "" ]; then echo "You must specify matrix size and block size"; exit 1; fi

i=$1
k=$2
set -x
./bmm-seq $i $k
