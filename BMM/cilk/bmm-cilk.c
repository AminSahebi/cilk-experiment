/*******************************************************************************
 *        CILK Matrix Multiply                                                 *
 *                                                                             *
 *  AUTHOR: Stefano Viola & Roberto Giorgi - UNISI                             *
 * VERSION: 0.0.1                                                              *
 *    DATE: 09/09/2016 (dd/mm/yy)                                              *
 *                                                                             *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>

/*******************************************************************************
 * Definitions for ROI for COTSon
 ******************************************************************************/

/* COTSON Base for custom instruction definition */
/* IMPORTANT: keep in sync with AbAeterno: abaeterno/lib/cpuid_call.h */
#define _COTSON_CUSTOM_BASE	0x2DAF0000
#define _COTSON_CUSTOM_TRACER   0xFF
#define _XSMOPCODE(_opc)	(_COTSON_CUSTOM_BASE+(_opc))
#define _XSMOPCODE_I(_opc,_im)	(_COTSON_CUSTOM_BASE+((_im)<<8)+(_opc))
#define _PACKED8(_sw,_zo)       ((((_sw)&1)<<7)|((_zo)&0x7F))
#define _AVOL			__asm__ volatile
#define _RCLB  /*example: #define _RCLB :"%rax" // don't forget the colon! */
#define _XP(str)		#str
#define _AS0(_of)		_XP(prefetchnta _of(%%rax,%%rax))
#define _JHERE			";jmp 1f;1:"
#define _XSM_XZONESTART(_i)	_XSMOPCODE_I(_COTSON_CUSTOM_TRACER,_PACKED8(0,_i))
#define _XSM_XZONESTOP(_i)	_XSMOPCODE_I(_COTSON_CUSTOM_TRACER,_PACKED8(1,_i))
#define XSM_XZONESTART(_z)	_AVOL (_AS0(_XSM_XZONESTART(_z)) _JHERE:: _RCLB)
#define XSM_XZONESTOP(_z)	_AVOL (_AS0(_XSM_XZONESTOP(_z))  _JHERE:: _RCLB)

/******************************************************************************/
/* XSM PERFORMANCE COUNTERS FUNCTIONS */
/******************************************************************************/
#define xzonestart(_zone) XSM_XZONESTART(_zone)
#define xzonestop(_zone) XSM_XZONESTOP(_zone)
/******************************************************************************/

/*******************************************************************************
 * These definitions are modified by filter passed in mydse.sh
 * Plese not change the name!
 ******************************************************************************/
int N=256;
int BLOCKSZ=8;
#define XDATA_INT64

#ifdef XDATA_SINGLE
    #define DATA float
#endif
#ifdef XDATA_DOUBLE
    #define DATA double
#endif
#ifdef XDATA_INT64
    #define DATA uint64_t
#endif
/***** mydse.sh parms *********************************************************/

//#define VERBOSE 1

DATA *A, *B, *C;
uint64_t superchecksum;
int n = 0, csok, n_local;

struct timeval  tv;


void fill_matrix()
{
  int i,j;
  /****************************************************************/
  int cr, cc;
  srand(12345); // start always with same numbers in the matrices (optional)
#ifdef VERBOSE
  printf("[INFO]: Fill Matrix B...\n");
#endif

  for (i = 0; i < n; i++) {
      cr = 0;
      for (j = 0; j < n-1; j++) {
          int val1 = rand()&0xFF;
          cr = (cr + val1) &0xFF;
          B[i*n+j] = (DATA)val1;
      }
      B[i*n+n-1] = (DATA)cr;
  }

#ifdef VERBOSE
  printf("[INFO]: Fill Matrix A...\n");
#endif
  for (j = 0; j < n; j++) {
      cc = 0;
      for (i = 0; i < n-1; i++) {
          int val2 = rand()&0xFF;
          cc = (cc + val2) &0xFF;
          A[i*n+j] = (DATA)val2;
       }
       A[(n-1)*n+j] = (DATA)cc;
  }
  // Calculate the SuperCheckSum
  superchecksum = 0;
  for (i = 0; i < n; i++) {
      superchecksum =
         (superchecksum + (uint64_t)(A[(n-1)*n+i]) * (uint64_t)(B[i*n+n-1]))&0xFF;
  }
#ifdef VERBOSE
  printf("SUPER CHECKSUM: %ld - %ld\n",superchecksum,(superchecksum<<2)&0xFF);
#endif
  superchecksum=(superchecksum<<2)&0xFF;
}

void prepare()
{
  n=N;
  int num_workers = __cilkrts_get_nworkers();

#ifdef VERBOSE
  printf("[INFO]: START prepare()\n");
#endif
  printf("[INFO]: SIZE = %d  BLOCK SIZE = %d  WORKERS = %d\n", n, BLOCKSZ, num_workers);
  A = (DATA *) malloc(sizeof(DATA) * (n * n));
  B = (DATA *) malloc(sizeof(DATA) * (n * n));
  C = (DATA *) malloc(sizeof(DATA) * (n * n));
#ifdef VERBOSE
  printf("--- MEMORY ALLOCATION ---\n");
  printf("[VERBOSE]: sizeof(DATA) = %ld\n",sizeof(DATA));
  printf("[VERBOSE]: A = %ld \t@ %p\n",(sizeof(DATA) * (n * n)),A);
  printf("[VERBOSE]: B = %ld \t@ %p\n",(sizeof(DATA) * (n * n)),B);
  printf("[VERBOSE]: C = %ld \t@ %p\n",(sizeof(DATA) * (n * n)),C);
#endif
  /* Let each process initialize C to zero */
  for (int i=0; i<(n*n); i++) {
      C[i] = (DATA)0;
  }
  fill_matrix();
#ifdef VERBOSE
  printf("[INFO]: END prepare()\n");
#endif
}

int matrixMultiply(DATA *a, DATA *b, DATA *c, int n, int n_local) {
    int i, j, k;
    DATA t;
    for(i=0; i<n_local; i++) {
        for (j=0; j<n; j++) {
            t = (DATA)0;
            for (k=0; k<n; k++) {
                t += a[i*n + k] * b[k*n + j];
            }
            c[i*n + j] = t;
        }
    }
    
    return 0;
}

void compute()
{
  //int index;
  //int local_block=BLOCKSZ;
  //int remaning_block=n;
  int total_move = 0;
#ifdef VERBOSE
  printf("[INFO]: START compute()\n");
#endif
  for(int index=0; index<n; index+=BLOCKSZ)
  {
    int remaning_block=n;
    int local_block=BLOCKSZ;
    cilk_spawn matrixMultiply(A, B, C, n, local_block);
    remaning_block=( remaning_block - local_block );
#ifdef VERBOSE
    printf("remaning_block = %d - index = %d\n", remaning_block, index);
    fflush(stdout);
#endif
    if(remaning_block>BLOCKSZ)
    {
      local_block=BLOCKSZ;
    }
    else
    {
      local_block=remaning_block;
    }
    total_move = (total_move + (n * local_block));
    A = (A + (n * local_block));
    C = (C + (n * local_block));
  }
  cilk_sync;
  // Restore the pointers
  A = (A - total_move);
  C = (C - total_move);
#ifdef VERBOSE
  printf("TOTAL MOVE = %d\n", total_move);
  printf("\n[INFO]: END compute()\n");
#endif
}

void report()
{
  uint64_t xchecksum = 0L;
#ifdef VERBOSE
  printf("[INFO]: START report()\n");
#endif
  int i,j;
//#define PRINT_MATRIX 1
#ifdef PRINT_MATRIX
  printf("%s addr = %p\n","[INFO]: PRINT C MATRIX ",C );
  int n_sq = n * n;
  for (i=0; i<n_sq; i++) {
      printf("C[%3d]: %ld\t",i, C[i]);
      if (i%n == 0) printf("\n");
  }
  printf("\n\n%s\n","[INFO]: PRINT A MATRIX" );
  
  for (i=0; i<n_sq; i++) {
      printf("A[%3d]: %ld\t",i, A[i]);
      if (i%n == 0) printf("\n");
  }
  
  printf("\n\n%s\n","[INFO]: PRINT B MATRIX" );
  
  for (i=0; i<n_sq; i++) {
      printf("B[%3d]: %ld\t",i, B[i]);
      if (i%n == 0) printf("\n");
  }
#endif
  // Verify the SuperCheckSum
  printf("\nCheck the result of Matrix Multiply...\n");
  for (i = 0; i < n; i++) {   // i = row pointer
      for (j = 0; j < n; j++) { // j = column pointer
          xchecksum = (xchecksum + (int)(C[i*n + j])) &0xFF;
          //printf("[i*n + j])=%d\n", (i*n + j));
      }
  }
  printf("checksum: %ld - superchecksum: %ld\n", xchecksum, superchecksum);
  //printf("C[%d] = %10ld\n",(n*n), C[n*n]);
  if(xchecksum == superchecksum)
  {
    printf("*** %s ***\n","SUCCESS");
    printf("All workers done, goodbye\n");
  }
  else
  {
    printf("*** %s ***\n","FAILURE");
  }
#ifdef VERBOSE
  printf("[INFO]: END report()\n");
#endif
}

/***** main start *************************************************************/
int main(int argc, char *argv[]) {
	N = atoi(argv[1]);
	BLOCKSZ = atoi(argv[2]);
    prepare();
    gettimeofday(&tv, NULL);
    long long time_in_mill = 
             (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000 ; // convert tv_sec & tv_usec to millisecond
    long long start_time = time_in_mill;

//    xzonestart(1);
    compute();
 //   xzonestop(1);
    
    gettimeofday(&tv, NULL);
    time_in_mill = 
             (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000 ; // convert tv_sec & tv_usec to millisecond
    printf("[INFO]: EXECUTION TIME = %lld ms\n",(time_in_mill - start_time));

    report();
    return 0;
}
/***** main end ***************************************************************/
