#!/bin/bash

if [ "$3" = "" ]; then echo "You must specify matrix size, block size and number of workers"; exit 1; fi

i=$1
j=$2
k=$3
set -x
export CILK_NWORKERS=$3
./bmm-cilk $i $j $k
