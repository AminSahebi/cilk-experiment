#!/bin/bash

if [ "$3" = "" ]; then echo "You must specify at least Fibonacci index threshold and number of workers"; exit 1; fi

i=$1
j=$2
set -x
export CILK_NWORKERS=$3
./fib-cilk $i $j 
