/*******************************************************************************
 *        Sequential Matrix Multiply                                                 *
 *                                                                             *
 *  AUTHOR: Stefano Viola & Roberto Giorgi & Marco Procaccini- UNISI                             *
 * VERSION: 0.0.1                                                              *
 *    DATE: 09/09/2016 (dd/mm/yy)                                              *
 *                                                                             *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include "fibcheck.h"
/*******************************************************************************
 * Definitions for ROI for COTSon
 ******************************************************************************/

/* COTSON Base for custom instruction definition */
/* IMPORTANT: keep in sync with AbAeterno: abaeterno/lib/cpuid_call.h */

/*******************************************************************************
 * These definitions are modified by filter passed in mydse.sh
 * Plese not change the name!
 ******************************************************************************/
int N=40;
int THRESHOLD=5; 

#define RECFIB
/***** mydse.sh parms *********************************************************/

//#define VERBOSE 1

long int superchecksum, checksum;
int n = 0;
struct timeval tv;

int serialfib(int newN)
{
#ifdef RECFIB
	if(newN==-2) return -1;
	if(newN==-1) return 1;
	return newN < 2 ? newN : serialfib(newN-1) + serialfib(newN-2);
#else
	int a=-1,b=1;
	int i=-2;
	for(; i<newN;++i){
		int z=a+b;
		a=b;
		b=z;
	}
	return a;
#endif
}

long int fib( int newN,int newT)
{

	if( newN < newT)
	{
		return serialfib(newN);
	}else
	{
		long int x = fib(newN-1,newT);
		long int y = fib(newN-2,newT); 

#ifdef VERBOSE
		printf("x = %ld \n",x);
		printf("y = %ld \n",y);
#endif
		return x+y;
	}
}

void prepare()
{

#ifdef VERBOSE
	printf("[INFO]: START prepare()\n");
#endif
	printf("[INFO]: FIB = %d  THRESHOLD = %d\n", N, THRESHOLD);
#ifdef VERBOSE
	printf("[INFO]: END prepare()\n");
#endif
}


void compute()
{
#ifdef VERBOSE
	printf("[INFO]: START compute()\n");
#endif

	n=N;

	checksum=fib(n,THRESHOLD);

}

void report()
{
#ifdef VERBOSE
	printf("[INFO]: START report()\n");

	printf("Fibonacci Result: %ld\n",checksum);

#endif
	superchecksum=fibresult[N];
	printf("Fibonacci Check: %ld\n",superchecksum);


	if(checksum == superchecksum)
	{
		printf("*** %s ***\n","SUCCESS");
		printf("All workers done, goodbye\n");
	}
	else
	{
		printf("*** %s ***\n","FAILURE");
	}

#ifdef VERBOSE
	printf("[INFO]: END report()\n");
#endif
}

/***** main start *************************************************************/
int main(int argc, char *argv[]) {
	N = atoi(argv[1]);
	THRESHOLD = atoi(argv[2]);
	prepare();
	gettimeofday(&tv, NULL);
	long long time_in_mill = 
		(tv.tv_sec) * 1000000 + (tv.tv_usec) ; // convert tv_sec & tv_usec to millisecond
	long long start_time = time_in_mill;

	compute();

	gettimeofday(&tv, NULL);
	time_in_mill = 

		(tv.tv_sec) * 1000000 + (tv.tv_usec) ; // convert tv_sec & tv_usec to millisecond
	printf("[INFO]: EXECUTION TIME = %lld us\n",(time_in_mill - start_time));

	report();
	return 0;
}
/***** main end ***************************************************************/
