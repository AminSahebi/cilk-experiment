#!/bin/bash
if [ "$1" = "" ]; then echo "You must specify number of processors, Fibonacci index and threashold"; exit 1; fi

i=$1
j=$2
k=$3
mpirun -n $i --hostfile ~/hosts ./fib-mpi $j $k


