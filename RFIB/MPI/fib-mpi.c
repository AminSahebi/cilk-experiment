#include <stdio.h>
#include <mpi.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#define _DEBUG_

/*******************************************************************************
 * Definitions for ROI
 ******************************************************************************/

/* COTSON Base for custom instruction definition */
/* IMPORTANT: keep in sync with AbAeterno: abaeterno/lib/cpuid_call.h */
#define _COTSON_CUSTOM_BASE	0x2DAF0000
#define _COTSON_CUSTOM_TRACER   0xFF
#define _XSMOPCODE(_opc)	(_COTSON_CUSTOM_BASE+(_opc))
#define _XSMOPCODE_I(_opc,_im)	(_COTSON_CUSTOM_BASE+((_im)<<8)+(_opc))
#define _PACKED8(_sw,_zo)       ((((_sw)&1)<<7)|((_zo)&0x7F))
#define _AVOL			__asm__ volatile
#define _RCLB  /*example: #define _RCLB :"%rax" // don't forget the colon! */
#define _XP(str)		#str
#define _AS0(_of)		_XP(prefetchnta _of(%%rax,%%rax))
#define _JHERE			";jmp 1f;1:"
#define _XSM_XZONESTART(_i)	_XSMOPCODE_I(_COTSON_CUSTOM_TRACER,_PACKED8(0,_i))
#define _XSM_XZONESTOP(_i)	_XSMOPCODE_I(_COTSON_CUSTOM_TRACER,_PACKED8(1,_i))
#define XSM_XZONESTART(_z)	_AVOL (_AS0(_XSM_XZONESTART(_z)) _JHERE:: _RCLB)
#define XSM_XZONESTOP(_z)	_AVOL (_AS0(_XSM_XZONESTOP(_z))  _JHERE:: _RCLB)


/*******************************************************************************
 * These definitions are modified by filter passed in mydse.sh
 * Plese not change the name!
 ******************************************************************************/


#define N 9
#define thd 2
#define RECFIB

#define XDATA_INT64

#ifdef XDATA_SINGLE
#define DATA float
#define MPI_DATATYPE MPI_FLOAT
#endif
#ifdef XDATA_DOUBLE
#define DATA double
#define MPI_DATATYPE MPI_DOUBLE
#endif
#ifdef XDATA_INT64
#define DATA uint64_t
#define MPI_DATATYPE MPI_UINT64_T
#endif


struct timespec tv;
long long start_time, time_in_nano;

static void xzonestart(int zone)
{
	printf("XZONESTART %d DETECTED\n",zone);
	clock_gettime(CLOCK_REALTIME, &tv);
	time_in_nano= (tv.tv_sec) * 1000 + (tv.tv_nsec)/1e6;
	start_time = time_in_nano;

}

static void xzonestop(int zone)
{
	clock_gettime(CLOCK_REALTIME, &tv);
	time_in_nano= (tv.tv_sec) * 1000 + (tv.tv_nsec)/1e6;
	printf("XZONESTOP %d DETECTED \n",zone);
	printf("[INFO]: EXECUTION TIME = %lld ms\n",time_in_nano - start_time);
	fflush(stdout);
}

void prepare();
void compute(char **argv);
void report();


//global variables

DATA fibn;
int universe_size;
int myrank; 
//int thd;
char command[255];
int world_size, flag;
int wid;


//MPI environments variables
MPI_Info local_info;
MPI_Status status;
MPI_Request send;
MPI_Comm parent;

char name[MPI_MAX_PROCESSOR_NAME];
int len_name;



long serialfib(int ser_n)
{
#ifdef RECFIB
	if(ser_n==-2) return -1;
	if(ser_n==-1) return 1;
	return ser_n < 2 ? ser_n : serialfib(ser_n-1) + serialfib(ser_n-2);
#else
	int a=-1,b=1;
	int i=-2;
	for(; i<ser_n;++i)
	{   
		int z=a+b;
		a=b;
		b=z;
	}
	return a;
#endif
}


void report()
{

	printf("[INFO]: START report()\n");
#ifdef _DEBUG_
	printf("Fibonacci result: %ld\n",fibn);
#endif

	DATA superchecksum = serialfib(N);
	printf("Fibonacci Check: %ld\n",superchecksum);

	if(fibn = superchecksum)
	{
		printf("*** %s ***\n","SUCCESS");
		printf("All workers done, goodbye\n");
	}
	else
	{
		printf("*** %s ***\n","FAILURE");
	}

	printf("[INFO]: END report()\n");

	fflush(stdout);
}




void prepare()
{

	MPI_Comm_size (MPI_COMM_WORLD, &wid);

	if (wid == 1)
	{
		printf("Just Master! not enough processors < 1\n");
		exit(0);
	}
	MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
	MPI_Info_create (&local_info);
MPI_Comm_get_parent(&parent);
	if (parent == MPI_COMM_NULL)
		wid=0;

	universe_size = 500;
	//thd = THRESHOLD;

	MPI_Comm_get_attr (MPI_COMM_WORLD, MPI_UNIVERSE_SIZE, &universe_size,&flag);

	if (universe_size == 1)
	{	
		perror ("No room to start workers");
		exit(0);
	}


#ifdef _DEBUG_
	MPI_Get_processor_name(name,&len_name);
	printf("Runnuning on node: %s\n",name);
#endif

}



void  master_fib(char **argv)
{

	MPI_Comm master_comm;

	int n_master=atoi(argv[1]);
	sprintf(command,"%s",argv[0]);

	argv+=1;	

	if (n_master < thd){
#ifdef _DEBUG_
		printf("MASTER: threashold reached: %d - %d\n",n_master,thd);
#endif
		fibn = serialfib(n_master);    
	}else{
		sprintf(argv[0],"%ld",n_master);
#ifdef _DEBUG_
		printf ("<root> spawning recursive process, n = %ld\n", atol(argv[0]));
#endif
		MPI_Comm_spawn (command, argv, 1, local_info, myrank, MPI_COMM_SELF,
				&master_comm, MPI_ERRCODES_IGNORE); 
		MPI_Recv (&fibn, 1, MPI_LONG, MPI_ANY_SOURCE, 1, master_comm,
				MPI_STATUS_IGNORE);

		MPI_Comm_disconnect(&master_comm);
	}
}



void slave_fib(char **argv)
{
	MPI_Comm children_comm[2];
	DATA x,y,n_slave;
	sprintf(command,"%s",argv[0]);
	argv += 1;
	n_slave  = atol (argv[0]);

	if (n_slave < thd)
	{
#ifdef _DEBUG_
		printf ("threshold reached: %ld - %d\n",n_slave,thd);
#endif
		DATA res = serialfib(n_slave);
		MPI_Isend (&res, 1, MPI_DATATYPE, 0, 1, parent,&send);
		MPI_Wait(&send,&status);
	}else{
#ifdef _DEBUG_
		printf ("<%ld> spawning new process (1)\n", n_slave);
#endif
		sprintf (argv[0], "%ld", (n_slave - 1));

		MPI_Comm_spawn (command, argv, 1, local_info, myrank,
				MPI_COMM_SELF, &children_comm[0], MPI_ERRCODES_IGNORE);
#ifdef _DEBUG_
		printf ("<%ld> spawning new process (2)\n", n_slave);
#endif

		sprintf (argv[0], "%ld", (n_slave - 2));

		MPI_Comm_spawn (command, argv, 1, local_info, myrank,
				MPI_COMM_SELF, &children_comm[1], MPI_ERRCODES_IGNORE);

		MPI_Recv (&x, 1, MPI_DATATYPE, MPI_ANY_SOURCE, 1,
				children_comm[0], MPI_STATUS_IGNORE);

		MPI_Recv (&y, 1, MPI_DATATYPE, MPI_ANY_SOURCE, 1,
				children_comm[1],MPI_STATUS_IGNORE);

		fibn = x + y;             // computation
		MPI_Isend (&fibn, 1, MPI_DATATYPE, 0, 1, parent,&send);
		MPI_Wait(&send,&status);
	}

	MPI_Comm_disconnect(&parent);
}


void compute(char **argv){

	if (myrank==0)
	{
		master_fib(argv);
	}else
	{
		slave_fib(argv);
	}
}

int main (int argc, char **argv)
{

	MPI_Init (&argc, &argv);
	//	flag_master=0;
	prepare();
	if(myrank==0)
		xzonestart(1);

	compute(argv);

	if(myrank==0)
		xzonestop(1);
	report();

MPI_Finalize();
return 0;
}


