#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int n, k, *array, *bins, data_count, num_bins;
/*
void histogram(int * data, int n, int * bins, int k){
	int num_bins = 800 / k;
	int number_of_threads = omp_get_max_threads();
	int local_bins[number_of_threads + 1][1024]; //for false sharing 
	memset(bins, 0, sizeof(bins));
	memset(local_bins, 0, sizeof(local_bins));
	{
		int id = omp_get_thread_num();
		int i, j;
		
		for(i = 0 ; i < n ; ++i)
			local_bins[id][data[i] / k] ++;

		for(i = 0 ; i < num_bins; ++i)
		{
			for(j = 0 ; j < number_of_threads; ++j)
			{
				bins[i] += local_bins[j][i];
			}
		}
	}
}
*/
void naive_histogram(int *data, int data_count, int *bins, int num_bins){
	memset(bins, 0, sizeof(bins));
	for(int i=0;i<data_count;i++)
	{
		bins[data[i]/num_bins]+=1;
		printf("bins[%d] = %d\n",i,bins[i]);
	}
}

int main(int argc, char *argv[])
{
	if (argc < 1 ){
	printf("please enter the data counts and number of bins\n");
	return 0;
	}
	data_count = atoi(argv[1]);
	num_bins = atoi (argv[2]);
	printf("computing histogram with data count %d and bin count %d\n",data_count,num_bins);
	array =(int*)malloc(sizeof(int) * data_count);
	bins =(int*)malloc(sizeof(int) * num_bins);
	for(int i=0;i<data_count;i++)
	{
		array[i] = rand()%num_bins;
	}
	//histogram(array, n, bins, k);
	naive_histogram(array, data_count, bins, num_bins);
	return 0;
}
