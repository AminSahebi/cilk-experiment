#!/bin/bash

if [ "$3" = "" ]; then echo "You must specify at least data count and bin count and number of workers"; exit 1; fi

i=$1
j=$2
set -x
export CILK_NWORKERS=$3
./hist-cilk $i $j 
