#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <pthread.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <time.h>


//#define DEBUG
typedef unsigned int uint;
typedef unsigned char uchar;

#define Cilk_lockvar pthread_mutex_t
#define Cilk_lock pthread_mutex_lock
#define Cilk_unlock pthread_mutex_unlock


int data_count, bin_count, size, *histogram, *color;

Cilk_lockvar *lock;


struct timespec tv;
long long start_time, time_in_micro;

static void xzonestart(int zone)
{
        printf("XZONESTART %d DETECTED\n",zone);
        clock_gettime(CLOCK_REALTIME, &tv);
        time_in_micro= (tv.tv_sec) * 1000000 + (tv.tv_nsec)/1e3;
        start_time = time_in_micro;

}

static void xzonestop(int zone)
{
        clock_gettime(CLOCK_REALTIME, &tv);
        time_in_micro= (tv.tv_sec) * 1000000 + (tv.tv_nsec)/1e3;
        printf("XZONESTOP %d DETECTED \n",zone);
        printf("[INFO]: EXECUTION TIME = %llu us \n",time_in_micro - start_time);
        fflush(stdout);
}


void prepare(){
	color = malloc(size * sizeof(int));
	histogram = malloc(bin_count * sizeof(int));
	lock  = (pthread_mutex_t *) calloc(bin_count,sizeof(pthread_mutex_t));
	for (int i = 0; i < size; ++i)
	{
		color[i] = (int)rand()%bin_count;
	}
#ifdef DEBUG
	for(int i=0; i< size; i++){
		printf("[INFO] colors[%d]\t%d\n",i, color[i]);
	}
#endif
}
void histo_cilk2(int *histogram, int *color, int size)
{
	if (size == 1) {
		Cilk_lock(&lock[*color]);
		histogram[*color]++;
		Cilk_unlock(&lock[*color]);
	}
	else {
		cilk_spawn histo_cilk2(histogram, color, size/2);
		cilk_spawn histo_cilk2(histogram, color + size/2, size - size/2);
		cilk_sync;
	}
} 
void compute(){

	histo_cilk2(histogram, color, size);
}

int main(int argc, char* argv[]){
	if(argc<2){
		printf("Please insert data_count and bin_count respectively\n");
		exit(1);
	}

	size = atoi (argv[1]);
	bin_count = atoi (argv[2]);	
	prepare();
	xzonestart(1);
	compute();
	xzonestop(1);
	printf("histogram size %d, bin_count %d\n",size, bin_count);
	return 0;
}
