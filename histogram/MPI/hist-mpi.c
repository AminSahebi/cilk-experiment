#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
//#define DEBUG

struct timespec tv;
long long start_time, time_in_micro;

static void xzonestart(int zone)
{
	printf("XZONESTART %d DETECTED\n",zone);
	clock_gettime(CLOCK_REALTIME, &tv);
	time_in_micro= (tv.tv_sec) * 1000000 + (tv.tv_nsec)/1e3;
	start_time = time_in_micro;

}

static void xzonestop(int zone)
{
	clock_gettime(CLOCK_REALTIME, &tv);
	time_in_micro= (tv.tv_sec) * 1000000 + (tv.tv_nsec)/1e3;
	printf("XZONESTOP %d DETECTED \n",zone);
	printf("[INFO]: EXECUTION TIME = %lld us\n",time_in_micro - start_time);
	fflush(stdout);
}


int nw;
int wid;
int data_count, bin_count, *slice, *histogram, *color;

/***************************** prepare ****************************************/
void prepare(){

	// Generate the sequence of random numbers in ther range [a, b].
	if(wid ==0) { 
		color = malloc(data_count * sizeof(int));
		histogram = malloc(bin_count * sizeof(int));
		for (int i = 0; i < data_count; ++i)
		{
			color[i] = (int)rand()%bin_count;
		}
	}
#ifdef DEBUG
	if(wid ==0) { 
		for(int i=0; i< data_count; i++){
			printf("[INFO] colors[%d]\t%d\n",i, color[i]);
		}
	}
	printf("[INFO] MPI RANK: %d\n",wid);fflush(stdout);
	printf("[INFO] MPI SIZE: %d\n",nw);fflush(stdout);
#endif
}

/************************ compute ************************************/
void compute(){
	int ssize = (data_count - 1) /nw + 1;
	int histogram_private[bin_count];
	for(int i=0; i<bin_count; i++){
		histogram_private[i] = 0;
	}
	slice = malloc(ssize * sizeof(int));
	/**
	 * MPI_Scatter(
	 *     void* send_data,
	 *     int send_count,
	 *     MPI_Datatype send_datatype,
	 *     void* recv_data,
	 *     int recv_count,
	 *     MPI_Datatype recv_datatype,
	 *     int root,
	 *     MPI_Comm communicator)
	 */

	MPI_Scatter(color, ssize, MPI_INT, slice, ssize, MPI_INT, 0, MPI_COMM_WORLD);
	// Processing data
	for (int i= 0; i<ssize; ++i){
		histogram_private[slice[i]]++; 
	}
	free(slice);
	/**
	 * MPI_Reduce(const void *sendbuf, void *recvbuf, int count,
	 *      MPI_Datatype datatype, MPI_Op op, int root,
	 *	MPI_Comm comm)
	 */
	for (int i=0; i<bin_count; i++){
		MPI_Reduce(&histogram_private[i], &histogram[i], 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	}
}

/************************ report ************************************/
void report(){
	if(wid ==0) { 
		printf("sequence length: %d\nnumber of bins: %d\n",data_count, bin_count);
		int sum=0;

		for (int i=0; i<bin_count;i++){
			sum+=histogram[i];
		}
		if(sum == data_count)
		{
			printf("*** %s ***\n","SUCCESS");fflush(stdout);
		}
		else
		{
			printf("*** %s ***\n","FAILURE");fflush(stdout);
		}
#ifdef DEBUG
		for (int i=0; i<bin_count;i++){
			printf("historgam[%d] %d\n",i,histogram[i]);
		}
#endif
		fflush(stdout);}
}
/***************************** main *************************************/
int main(int argc, char *argv[]) {
	if (argc < 3){
		printf("[INFO] insert the following data <color-count><bin-count>\n");
		exit(1);
	}
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nw);
	MPI_Comm_rank(MPI_COMM_WORLD, &wid);

	data_count = atoi(argv[1]);
	bin_count = atoi(argv[2]);

	prepare();
	if(wid == 0)
		xzonestart(1);
	compute();

	if(wid == 0)
		xzonestop(1);
	report();
	//	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	return 0;
}
/***** main end ***************************************************************/

