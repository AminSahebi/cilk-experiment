#!/bin/bash
if [ "$1" = "" ]; then echo "You must specify number of processors, number of colors and number of bins"; exit 1; fi

i=$1
j=$2
k=$3
#echo ""
#echo "Histogram running over $1 processors, #of colors $2, #of bins $3"
#echo ""
#AXM5 and AXM3 since there are other Ethernet Interfaces
#set -x
mpirun --mca btl_tcp_if_exclude docker0,127.0.0.0/8 -np $1 --hostfile ~/hosts ~/BENCHMARKS/histogram/MPI/hist-mpi $2 $3
#mpirun --mca btl_tcp_if_exclude docker0,127.0.0.0/8 --bind-to none --map-by ppr:1:node -np $1 --hostfile ~/hosts ~/BENCHMARKS/histogram/MPI/hist-mpi $2 $3
